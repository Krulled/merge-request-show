#Write a function to return the uppercase version of a letter
def toLowerCase(b):
    if ord(b) >= 65 and ord(b) <= 90:
        b = chr(ord(b)+32)
    return b
 
#Write a function to return the lowercase version of a letter       
def toUpperCase(i):
    if ord(i) >= 97 and ord(i) <= 122:
        i = chr(ord(i) - 32)
        return i
        
#Write a function that returns true if the letter is an alphabet
def trueAlpha(i):
    if(ord(i) >= 65 and ord(i) <= 122):
        return True;
    else:
        return False
    
def checkDigit(di):
        if(ord(di) >= 48 and ord(di) <= 57):
            return True
        else:
            return False
    
def checkSpecialCharacters(di):
    if((ord(di) >= 48 and ord(di) <= 57) or (ord(di) >= 65 and ord(di) <= 122)):
        return False
    else:
        return True
    
print(toUpperCase('a'))
print(toLowerCase("B"))
print(trueAlpha("a"))
print(checkDigit("s"))
print(checkSpecialCharacters("S"))